﻿using Puppies.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Puppies.Web.DAL
{
    public class PuppySqlDao : IPuppyDao
    {
        private readonly string connectionString;

        public PuppySqlDao(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Returns a list of all puppies
        /// </summary>
        /// <returns></returns>
        public IList<Puppy> GetPuppies()
        {
            IList<Puppy> puppies = new List<Puppy>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand("SELECT id, weight, name, gender, paper_trained FROM dbo.Puppies", connection);

                try
                {
                    connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        // chose empty constructor 
                        Puppy puppy = new Puppy();
                        puppy.Id = int.Parse(reader["id"].ToString());
                        puppy.Gender = reader["gender"].ToString();
                        puppy.Name = reader["name"].ToString();
                        puppy.Weight = int.Parse(reader["weight"].ToString());
                        puppy.PaperTrained = bool.Parse(reader["paper_trained"].ToString());
                        puppies.Add(puppy);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                }

                return puppies;
            }
        }

        /// <summary>
        /// Returns a specific puppy
        /// </summary>
        /// <returns></returns>
        public Puppy GetPuppy(int id)
        {
            Puppy puppy = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand("SELECT id, weight, name, gender, paper_trained FROM dbo.Puppies WHERE id = @id", connection);
                command.Parameters.AddWithValue("@id", id);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        puppy = new Puppy();
                        puppy.Id = int.Parse(reader["id"].ToString());
                        puppy.Gender = reader["gender"].ToString();
                        puppy.Name = reader["name"].ToString();
                        puppy.Weight = int.Parse(reader["weight"].ToString());
                        puppy.PaperTrained = bool.Parse(reader["paper_trained"].ToString());
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                }

                return puppy;
            }
        }

        /// <summary>
        /// Saves a new puppy to the system.
        /// </summary>
        /// <param name="newPuppy"></param>
        /// <returns></returns>
        public void SavePuppy(Puppy newPuppy)
        {
            string insertPuppy = "INSERT INTO dbo.Puppies (weight, name, gender, paper_trained ) VALUES ( @weight, @name, @gender, @paper_trained )";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(insertPuppy, connection);
                command.Parameters.AddWithValue("@weight", newPuppy.Weight);
                command.Parameters.AddWithValue("@name", newPuppy.Name);
                command.Parameters.AddWithValue("@gender", newPuppy.Gender);
                command.Parameters.AddWithValue("@paper_trained", newPuppy.PaperTrained);

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}
