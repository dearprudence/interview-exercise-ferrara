﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Puppies.Web.DAL;
using Puppies.Web.Models;

namespace Puppies.Web.Controllers
{
    public class PuppiesController : Controller
    {
        private IPuppyDao _puppyDao;

        public PuppiesController(IPuppyDao puppyDao )
        {
            _puppyDao = puppyDao;
        }

        [HttpGet]
        public ViewResult Detail(int id)
        {
            Puppy puppy = _puppyDao.GetPuppy(id);
            return View(puppy);
        }

        [HttpGet]
        public ViewResult Index()
        {
            IList<Puppy> puppies = _puppyDao.GetPuppies();
            return View(puppies);
        }

        [HttpPost]
        public RedirectResult Save(Puppy newPuppy)
        {
            _puppyDao.SavePuppy(newPuppy);
            return Redirect("Index");
        }
    }
}
